import useSWR from 'swr'

export function useGetNearest ({ lng = 19.940984, lat = 50.062006, distance = 100, lang = 'pl'} = {}) {
  //todo use lang
  const { data, error } = useSWR(`/api/airly/nearest?lng=${lng}&lat=${lat}&distance=${distance}`, { revalidateOnFocus: false })
  return {
    data,
    error,
    isLoading: !error && !data
  }
}
