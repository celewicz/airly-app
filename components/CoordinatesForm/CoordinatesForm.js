import styles from '../../styles/Home.module.css'

export function CoordinatesForm({handleSubmit, coordinates, handleOnChange, isValid, distance, lang}){
  return(
    <form onSubmit={handleSubmit} onChange={handleOnChange} className={styles.formWrapper}>
      <div className={styles.card}>
        <label htmlFor="lat">Latitude</label>
        <input type='text' id='lat' defaultValue={coordinates?.latitude}  pattern="[0-9\.]+" />
      </div>
      <div className={styles.card}>
        <label htmlFor="lng">Longitude</label>
        <input type='text' id='lng'  defaultValue={coordinates?.longitude}  pattern="[0-9\.]+" />
      </div>
      <div className={styles.card}>
        <label htmlFor="lang">Language</label>
        <select name="lang" id="lang" defaultValue={lang}>
          <option value="en">EN</option>
          <option value="pl">PL</option>
        </select>
        <label htmlFor="lang">Distance ( km )</label>
        <select name="distance" id="distance" defaultValue={distance}>
          <option value="50">50 km</option>
          <option value="100">100 km</option>
          <option value="200">200 km</option>
          <option value="300">300 km</option>
        </select>
      </div>
      <div className={styles.card}>
        <input type='submit' value={'submit'} disabled={!isValid} />
      </div>
    </form>
  )
}
