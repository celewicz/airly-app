export const installation = [
  {
    "id": 7949,
    "location": {
      "latitude": 51.864805,
      "longitude": 20.877829
    },
    "locationId": 7949,
    "address": {
      "country": "Poland",
      "city": "Grójec",
      "street": "Józefa Piłsudskiego",
      "number": "47",
      "displayAddress1": "Grójec",
      "displayAddress2": "Józefa Piłsudskiego"
    },
    "elevation": 152.51,
    "airly": true,
    "sponsor": {
      "id": 35,
      "name": "Celsium",
      "description": "Airly Sensor's sponsor",
      "logo": "https://cdn.airly.eu/logo/Celsium.jpg",
      "link": null,
      "displayName": "Celsium"
    }
  }
]
