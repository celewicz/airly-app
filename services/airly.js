import axios from 'axios'

// 1 apiKey : hk0ueUdgnQrotRUhVUdceYDdpDzNBEfj
// 2 apiKey : YyLDFz3zLFLHLw4RTb3DMKWaD2PciT9h

const api = axios.create({
  baseURL: 'https://airapi.airly.eu/v2',
  responseType: 'json',
  headers: { 'apikey': 'hk0ueUdgnQrotRUhVUdceYDdpDzNBEfj' }
})
// todo move url and apiKey to env variables

export const getNearest = async ({lat, lng, distance}) => {
  return api.get(`installations/nearest?lat=${lat}&lng=${lng}&maxDistanceKM=${distance}&maxResults=1`)
}
