import {getNearest} from '../../../services/airly'

export default (req, res) => {
  const { lng, lat, distance } = req.query
  res.statusCode = 200
  res.setHeader('Content-Type', 'application/json')
  getNearest({lat, lng, distance})
    .then(r => res.end(JSON.stringify(r.data)))
    .catch(error => error)
}

export const config = {
  api: {
    externalResolver: true
  }
}
