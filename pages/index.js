import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { useCallback, useEffect, useState } from 'react'
import { useGetNearest } from '../hooks/useGetNearest'
import { installation } from '../utils/mockData'
import { isLatitude, isLongitude, isValidCoordinates } from '../utils/helpers'
import {CoordinatesForm} from '../components/CoordinatesForm/CoordinatesForm'
import {MapBox} from '../components/MapBox/MapBox'

export default function Home() {
  const [coordinates, setCoordinates] = useState({});
  const [distance, setDistance] = useState(50)
  const [lang, setLang] = useState('en')
  const [newLat, setNewLat] = useState();
  const [newLng, setNewLng] = useState();
  const [hasValidData, setHasValidData] = useState(false)
  const [error, setError] = useState({});
  useEffect(() => {
    if(!navigator.geolocation) {
      console.log('geolocation not supported')
    } else {
      navigator.geolocation.getCurrentPosition(onLocationSuccess, onLocationError);
    }
  }, []);

  function onLocationSuccess(position) {
    const {latitude, longitude} = position.coords;
    setCoordinates({longitude, latitude})
  }

  function onLocationError() {
    const error = {message: 'Unable to get geoLocalization data' }
    setError(error);
  }

  const handleOnChange = (e) => {
    let {latitude, longitude} = coordinates;
    if(e.target.id === 'lat') latitude = e.target.value;
    if(e.target.id === 'lng') longitude = e.target.value;
    if(e.target.id === 'distance') setDistance(e.target.value);
    if(e.target.lang === 'lang') setLang(e.target.value);
    if(isValidCoordinates({latitude, longitude})) {
      setNewLng(longitude)
      setNewLat(latitude)
      setHasValidData(true);
    } else {
      setHasValidData(false);
    }
  }


  const handleOnMapClick = useCallback((_, data)=>{
    const {lng: longitude, lat: latitude} = data.lngLat
    setCoordinates({longitude, latitude})

  },[])

  const handleSubmit = (e) => {
    e.preventDefault()
    if(isLongitude(newLng) && isLatitude(newLat))  {
       setHasValidData(true);
       const newCoordinates = {latitude: newLat, longitude: newLng}
       setCoordinates(newCoordinates)
    } else {
      setError({message: 'Please provide correct coordinates data'})
    }
  }

  const {data: nearestData, error: nearestError,  isLoading} = useGetNearest({ lng: coordinates?.longitude, lat: coordinates?.latitude, distance, lang })

  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}> Airly app</h1>

        <CoordinatesForm
          handleOnChange={handleOnChange}
          handleSubmit={handleSubmit}
          coordinates={coordinates}
          isValid={hasValidData}
          distance={distance}
        />

        {error.message && <div className={styles.errorBox}>{error.message}</div> }

        { coordinates.latitude && coordinates.longitude &&  <MapBox
          longitude={coordinates.longitude}
          latitude={coordinates.latitude}
          handleOnMapClick={handleOnMapClick}
          />
        }

        {isLoading && <div > Loading data from Ailry </div>}

        {/*//todo handle 429 error from remote api*/}
        {nearestError && <div className={styles.errorBox}> Can't load data from Ailry </div>}

        <div className={styles.grid}>
          <div className={styles.card}>
            <pre>{JSON.stringify(nearestData, null, ' ')}</pre>
          </div>

        </div>


      </main>

      <footer className={styles.footer}>

      </footer>
    </div>
  )
}

