import ReactMapboxGl, { Layer, Feature, Marker } from 'react-mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import styles from '../../styles/Map.module.css'



const Map = ReactMapboxGl({
  accessToken: 'pk.eyJ1IjoiaGltbWVsYnViIiwiYSI6ImNraHAwemRlbzNic3Iyc2w2eHhrZTRwdTYifQ.O2v7P4DOQcUixa8dm0PldA'
});

export function MapBox({longitude, latitude, handleOnMapClick}){
  const coordinates = [longitude, latitude]
  return (
    <Map
      style="mapbox://styles/mapbox/streets-v9"
      containerStyle={{
        height: '40vh',
        width: '40vw'
      }}
      center={coordinates}
      onClick={handleOnMapClick}
    >
      <Marker
        coordinates={coordinates}
        anchor="bottom">
        <div className={styles.dot}></div>
      </Marker>
    </Map>
  )
}
