import '../styles/globals.css'
import axios from 'axios'
import { SWRConfig } from 'swr'
import Head from 'next/head'

function MyApp({ Component, pageProps }) {

  return (
    <SWRConfig
      value={{
        fetcher: url => axios.get(url).then(res => res.data)
      }}
    >
      <Head>
        <title>Map app</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link
          href="https://api.mapbox.com/mapbox-gl-js/v1.10.1/mapbox-gl.css"
          rel="stylesheet"
        />
      </Head>
      <Component {...pageProps} />
    </SWRConfig>
  )
}

export default MyApp
